﻿using System.Collections.Generic;
using System.Linq;
using AutoFixture;
using AutoFixture.AutoNSubstitute;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace WordFinder.UnitTests
{
    [TestClass]
    public sealed class WordFinderTests
    {
        private readonly IFixture _fixture;
        private readonly IMatrixBuilder _matrixBuilder;
        private readonly ILogger _logger;
        private readonly WordFinder _sut;

        public WordFinderTests()
        {
            _fixture = new Fixture().Customize(new AutoNSubstituteCustomization { ConfigureMembers = true });
            _matrixBuilder = _fixture.Create<IMatrixBuilder>();
            _logger = _fixture.Create<ILogger>();
            _sut = new WordFinder(_matrixBuilder, _logger);
        }

        [TestMethod]
        public void Find_MatchesHorizontally_VerifyLogger()
        {
            // Arrange
            var wordsToSearch = new List<string> { "HI" };
            var matrix = new char[2, 2];
            matrix[0, 0] = 'H';
            matrix[0, 1] = 'I';
            matrix[1, 0] = 'L';
            matrix[1, 1] = 'B';
            _matrixBuilder.BuildMatrix().Returns(matrix);

            // Act
            _sut.Find(wordsToSearch);

            // Assert
            _logger.Received(1).LogDebug("HI was found horizontally");
        }

        [TestMethod]
        public void Find_MatchesVertically_VerifyLogger()
        {
            // Arrange
            var wordsToSearch = new List<string> { "HI" };
            var matrix = new char[2, 2];
            matrix[0, 0] = 'H';
            matrix[0, 1] = 'O';
            matrix[1, 0] = 'I';
            matrix[1, 1] = 'B';
            _matrixBuilder.BuildMatrix().Returns(matrix);

            // Act
            _sut.Find(wordsToSearch);

            // Assert
            _logger.Received(1).LogDebug("HI was found vertically");
        }

        [TestMethod]
        public void Find_NoMatches_VerifyEmptyResult()
        {
            // Arrange
            var wordsToSearch = new List<string> { "test" };
            var matrix = new char[1, 1];
            matrix[0, 0] = 'A';
            _matrixBuilder.BuildMatrix().Returns(matrix);

            // Act
            var actual = _sut.Find(wordsToSearch);

            // Assert
            Assert.AreEqual(actual.Count(), 0);
        }

        [TestMethod]
        public void Find_MatchesOnce_VerifyResult()
        {
            // Arrange
            var wordsToSearch = new List<string> { "H" };
            var matrix = new char[1, 1];
            matrix[0, 0] = 'H';
            _matrixBuilder.BuildMatrix().Returns(matrix);

            // Act
            var actual = _sut.Find(wordsToSearch);

            // Assert
            Assert.AreEqual(actual.Count(), 1);
        }

        [TestMethod]
        public void Find_MultipleMatches_VerifyCountResult()
        {
            // Arrange
            var wordsToSearch = new List<string> { "A", "B" };
            var matrix = new char[2, 2];
            matrix[0, 0] = 'A';
            matrix[0, 1] = 'B';
            matrix[1, 0] = 'A';
            matrix[1, 1] = 'C';
            _matrixBuilder.BuildMatrix().Returns(matrix);

            // Act
            var actual = _sut.Find(wordsToSearch);

            // Assert
            Assert.AreEqual(actual.Count(), 3);
        }
    }
}
