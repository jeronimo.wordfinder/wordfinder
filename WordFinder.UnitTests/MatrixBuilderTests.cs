using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WordFinder.UnitTests
{
    [TestClass]
    public sealed class MatrixBuilderTests
    {
        [TestMethod]
        public void BuildMatrix_VerifyResult()
        {
            // Arrange
            var matrixBuilder = new MatrixBuilder();

            // Act
            var actual = matrixBuilder.BuildMatrix();

            // Assert
            Assert.AreEqual(actual.Length, MatrixBuilder.MatrixSize* MatrixBuilder.MatrixSize);
        }
    }
}
