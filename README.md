# WordFinder
Presented with a character matrix and a large stream of words, your task is to create a Class
that searches the matrix to look for the words from the word stream. Words may appear
horizontally, from left to right, or vertically, from top to bottom. 

The WordFinder constructor receives a set of strings which represents a character matrix. The
matrix size needs to be 64x64, all strings contain the same number of characters.
The "Find" method should return the top 10 most repeated words from the word stream found in
the matrix. If no words are found, the "Find" method should return an empty set of strings. If any
word in the word stream is found more than once within the matrix, the search results should
count it only once.

##
Analysis: 
* The matrix should be auto generated and not enter by user given the size. 
* We are not taking in consideration that words can appear horizontally from right to left
* We are not taking in consideration that words can appear vertically from bottom to top
* We are not taking in consideration that words can appear in diagonals
* We need to count every appear for each word

To be able to test the functionality writing unit tests I created a couple of interfaces and use dependency inyection so unit testing is easier.
