﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace WordFinder
{
    public class WordFinder
    {
        private readonly ILogger _logger;
        private readonly IMatrixBuilder _matrixBuilder;

        private char[,] _matrix;
        public WordFinder(IMatrixBuilder matrixBuilder, ILogger logger)
        {
            _logger = logger;
            _matrixBuilder = matrixBuilder;
        }

        public IEnumerable<string> Find(IEnumerable<string> wordStream)
        {
            _matrix = _matrixBuilder.BuildMatrix();
            var matrixSize = Math.Sqrt(_matrix.Length);
            var rowCount = matrixSize;
            var columnCount = matrixSize;
            var result = new List<string>();
            var wordStreamList = wordStream.ToList();
            for (var rowIndex = 0; rowIndex < rowCount; rowIndex++)
            {
                for (var columnIndex = 0; columnIndex < columnCount; columnIndex++)
                {
                    foreach (var word in wordStreamList.Where(word => _matrix[rowIndex, columnIndex] == word.ToUpper().Trim().FirstOrDefault()))
                    {
                        var sanitizeWord = word.ToUpper().Trim();
                        // horizontal
                        if (columnIndex + word.Length <= columnCount && IsWordLeftToRight(rowIndex, columnIndex, sanitizeWord))
                        {
                            result.Add(sanitizeWord);
                            _logger.LogDebug($"{word} was found horizontally");
                        }
                        // vertical
                        else if (rowIndex + word.Length <= rowCount && IsWordTopToBottom(rowIndex, columnIndex, sanitizeWord))
                        {
                            result.Add(sanitizeWord);
                            _logger.LogDebug($"{word} was found vertically");
                        }
                    }
                }
            }

            return result;
        }

        private bool IsWordTopToBottom(int rowIndex, int columnIndex, string word)
        {
            for (var i = 1; i < word.Length; i++)
            {
                if (word[i] != _matrix[rowIndex + i, columnIndex])
                {
                    return false;
                }
            }

            return true;
        }

        private bool IsWordLeftToRight(int rowIndex, int columnIndex, string word)
        {
            for (var i = 1; i < word.Length; i++)
            {
                if (word[i] != _matrix[rowIndex, columnIndex + i])
                {
                    return false;
                }
            }

            return true;
        }
    }
}