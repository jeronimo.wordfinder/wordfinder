﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;

namespace WordFinder
{
    class Program
    {
        static void Main(string[] args)
        {
            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);
            var serviceProvider = serviceCollection
                .AddLogging()
                .AddSingleton<IMatrixBuilder, MatrixBuilder>()
                .BuildServiceProvider();

            var logger = serviceProvider.GetService<ILogger<Program>>();
            logger.LogInformation("Starting Word Finder Application");

            var matrixService = serviceProvider.GetService<IMatrixBuilder>();
            var wordFinder = new WordFinder(matrixService, serviceProvider.GetService<ILogger<WordFinder>>());
            Console.WriteLine("Enter the words to find into the matrix (64x64), separate by comma. Enter to search");
            Console.WriteLine();
            var words = Console.ReadLine();
            while (!Regex.IsMatch(words!, @"^[a-zA-Z, ]+$", RegexOptions.IgnoreCase))
            {
                logger.LogError("The words should contain only letters. Re enter the words:");
                words = Console.ReadLine();
            }

            var singleWords = words.Split(',');
            logger.LogInformation("Top 10 most repeated words:");
            Console.WriteLine();
            var result = wordFinder.Find(singleWords).ToList();
            if (!result.Any())
            {
                logger.LogWarning("No words matched!");
            }
            else
            {
                var mostRepeated = result
                    .GroupBy(s => s)
                    .OrderByDescending(g => g.Count());
                mostRepeated.Take(10).ToList()
                    .ForEach(g => Console.WriteLine("{0} - occurrences: {1}", g.Key, g.Count()));
            }

            logger.LogInformation("All done!");
            serviceProvider.Dispose();
        }
        private static void ConfigureServices(IServiceCollection services)
        {
            services.AddLogging(configure => configure.AddConsole())
                .AddTransient<Program>()
                .AddTransient<WordFinder>();
        }
    }
}
