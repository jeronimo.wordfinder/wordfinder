﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WordFinder
{
    public interface IMatrixBuilder
    {
        char[,] BuildMatrix();
    }
    public sealed class MatrixBuilder : IMatrixBuilder
    {
        public const int MatrixSize = 64;

        public char[,] BuildMatrix()
        {
            var rows = BuildRandomStrings().ToArray();

            var rowCount = rows.Length;
            var columnCount = rows[0].Length;

            var matrix = new char[rowCount, columnCount];

            for (var row = 0; row < rowCount; row++)
            {
                for (var column = 0; column < columnCount; column++)
                {
                    matrix[row, column] = rows[row][column];
                }
            }

            return matrix;
        }
        private IEnumerable<string> BuildRandomStrings()
        {
            var matrix = new List<string>();
            for (var i = 0; i < MatrixSize; i++)
            {
                matrix.Add(RandomString(MatrixSize));
            }
            return matrix;
        }

        private static readonly Random Random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[Random.Next(s.Length)]).ToArray());
        }
    }


}
